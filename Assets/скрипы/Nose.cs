using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nose : MonoBehaviour
{
    [SerializeField] private string groundLayerName = "Ground";
    [SerializeField] private SimpleEnemy simpleEnemy;
    private void OnTrigger2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(groundLayerName)) return;

        simpleEnemy.Flip();
    }
}
