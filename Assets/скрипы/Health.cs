using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    private Game game;

    private Vector3 startPosition;

    private void Awake()
    {
        game = FindObjectOfType<Game>();
        if (game == null) Debug.Log("� �� ����� GAME!:( ����");
    }
    private void Start()
    {
        startPosition = playerTransform.position;
    }
    public void TakeDamage()
    {
        game.LoseLife();
        playerTransform.position = startPosition;
    }
}
